//
//  NetworkServices.swift
//  M16NNdM17
//
//  Created by Abraam on 10.01.2022.
//
import Foundation
class NetworkServices {
    var categoryList = [MovieResult]()
func request(urlString:String,complition:@escaping(Result<SearchResponse,Error>) ->Void ){
    guard let url = URL(string: urlString) else {return}
    URLSession.shared.dataTask(with: url) { (data,response,error) in
        DispatchQueue.main.async {
            if let error = error {
                print("Error")
               
                complition(.failure(error))
                return
            }
            guard let data = data else {return}
            do {
                let movies = try JSONDecoder().decode(SearchResponse.self, from: data)
                complition(.success(movies))
               
                
            }catch let jsonError{
                print("Failed to decode to json", jsonError)
                complition(.failure(jsonError))
            }
            
        }
    }.resume()

}
}
