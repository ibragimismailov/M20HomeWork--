//
//  SearchRespone.swift
//  M16NNdM17
//
//  Created by Abraam on 10.01.2022.
//

import Foundation
import UIKit
struct SearchResponse:Decodable {
    var searchType:String
    var expression:String
    var results:[MovieResult]
}
struct MovieResult:Decodable {
    var id:String
    var resultType:String
    var image:String
    var title:String
    var description:String
}
struct ErrorMessage {
    var errorMessage:String
}

extension UIImageView {
    func downloadedFrom(url:URL,contentMode mode:UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url){(data,response,error) in
            guard let httpUrlResponse = response as? HTTPURLResponse,httpUrlResponse.statusCode == 200,
                  let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                  let data = data, error == nil,
                  let image = UIImage(data: data)
            else{return}
            DispatchQueue.main.async {
                self.image = image
            }
        }.resume()
    }
    func downloadedFromm(link:String,contentMode mode : UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link)else {return}
            downloadedFrom(url:url ,contentMode: mode)
        
    }
}
