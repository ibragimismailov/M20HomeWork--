//
//  ViewController.swift
//  M16NNdM17
//
//  Created by Abraam on 27.12.2021.
//


import UIKit


class ViewController: UIViewController {
 
private var collectionView:UICollectionView?
 let searchController = UISearchController(searchResultsController: nil)
    
 var searchResponse:SearchResponse?
 var categoryList = [SearchResponse]()
 let searchmovies   = [MovieResult]()
 var networkServices = NetworkServices()
 private var timer:Timer?
    
override func viewDidLoad() {
super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 177, height: 319)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView?.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    guard let collectionView = collectionView else {
    return
 }
        collectionView.dataSource = self
        collectionView.delegate = self
   
        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
        collectionView.frame = view.bounds
        view.addSubview(collectionView)
    
    let urlString = "https://imdb-api.com/API/Search/k_az1ipaoz/star"
    timer?.invalidate()
    timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (_) in
        self.networkServices.request(urlString: urlString) {[weak self] (result) in
    switch result{
    case .success(let searchResponse):
        self?.searchResponse = searchResponse
        self?.collectionView?.reloadData()
    case .failure(let error):
              print(error)
   }
            
  }
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
        
    }

        setupSearchBar()
    
   
 }
   

    private func setupSearchBar(){
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
  }

 }
    extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource {
            
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchResponse?.results.count ?? 0
 }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
   
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as!CustomCollectionViewCell
        let imageUrlString = (searchResponse?.results[indexPath.row].image)!
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 0.3
        let movies = searchResponse?.results[indexPath.row]
        
        cell.movieName.text = movies?.title
        cell.watchButton.setTitle(movies?.description, for: .normal)
        navigationItem.title = movies?.title
        cell.configure(with: imageUrlString)
       
        cell.watchButton.addTarget(self, action: #selector(gotoSecond), for: .touchUpInside)
    return cell
  }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.performSegue(withIdentifier: "details", sender: indexPath.row)
            
    
     }

//
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let index = sender as? Int
            let vc = segue.destination as! DetailViewControllerr
            vc.products = searchResponse?.results[index!]

            }
        }
        
        
 
    
  extension  ViewController {
    @objc func gotoSecond () {
        let rootVC = SecondViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true, completion: nil)
 }
}
extension ViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        let urlString = "https://imdb-api.com/API/Search/k_az1ipaoz/\(searchText)"
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (_) in
            self.networkServices.request(urlString: urlString) {[weak self] (result) in
        switch result{
        case .success(let searchResponse):
            self?.searchResponse = searchResponse
            self?.collectionView?.reloadData()
        case .failure(let error):
                  print(error)
       }
                
      }
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
            
        }
        
 

    }
        
}




