//
//  SecondViewControllerr.swift
//  M16NNdM17
//
//  Created by Abraam on 13.01.2022.
//

import UIKit

class DetailViewControllerr: UIViewController {

    @IBOutlet weak var secondId: UILabel!
    @IBOutlet weak var seconddescprip: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    @IBOutlet weak var secondImage: UIImageView!
    var products:MovieResult?
    var netService = NetworkServices()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let  p = products {
            
            configure(with: MovieResult.init(id: p.id , resultType: "", image: p.image, title: p.title, description: p.description))
        }
       
    

           
   }
    func configure (with model:MovieResult) {
        self.secondId.text  = model.id
        self.seconddescprip.text = model.description
        self.secondTitle.text = model.title
        let url = model.image
        if let data = try? Data(contentsOf: URL(string: url)!) {
            self.secondImage.image = UIImage(data: data)
        }
    }

 
                    
                
}

