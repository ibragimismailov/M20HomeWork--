//
//  SearchModel.swift
//  M16Project
//
//  Created by Abraam on 09.01.2022.
//

import Foundation

struct SearchType:Decodable {
    var searchType:String
    var expression:String
    var results:[MoviesData]?
}
struct MoviesData:Decodable {
    var id:String
    var resultType :String
    var title:String
    var description:String
    var image:String
}
