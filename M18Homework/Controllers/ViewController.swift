//
//  ViewController.swift
//  M16Project
//
//  Created by Abraam on 23.11.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let   searchController = UISearchController(searchResultsController: nil)
    var results = [MoviesData]()
    
override func viewDidLoad() {

super.viewDidLoad()
   
   // setupSearchBar()
    let urlString = "https://imdb-api.com/API/SearchTitle/k_az1ipaoz/star"
    request(urlString: urlString) { (SearchType,error) in
        SearchType?.results.map({ (aaaa) in
            print(aaaa)
        })
        
    }
 

    }
    func request(urlString:String,completion: @escaping(SearchType?,Error?) -> Void) {
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { (data,response,error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(nil,error)
                    print("Some Error: \(error.localizedDescription)")
                    return
                }
                guard let data = data else {return}
                do{
                    let json = try JSONDecoder().decode(SearchType.self, from: data)
                    completion(json,nil)
                }catch let jsonError{
                    print(jsonError)
                    completion(nil,jsonError)
                }
                

            }
        }.resume()
        
    }
    private func setupSearchBar () {
        navigationItem.searchController = searchController
       // searchController.searchBar.delegate = self
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.obscuresBackgroundDuringPresentation = false
            
    }
   

}
extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! MovieTableViewCell
        cell.movieLabel.text = "hello"
       
        return cell
    }
    
    
}
//extension ViewController:UISearchBarDelegate{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        print(searchText)
//    }
//}

