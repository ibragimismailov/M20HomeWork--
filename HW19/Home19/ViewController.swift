//
//  ViewController.swift
//  Home19
//
//  Created by Abraam on 16.01.2022.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
  
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var searchTextfield_LbL: UITextField!
    

   //var titleString = moan?.name
    var titleString2 = Decode(dictionary: ["":""])
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleString = ""
        print(titleString)
        

}
    func sendReguestWithAlamofire () {
        let item = Moan(birth: 1993, occupation: "developer", name: "ibragim", lastname: "ismailov", country: "Russia")
        result.text = item.name
        AF.request("https://jsonplaceholder.typicode.com/posts",
                   method: .post,
                   parameters: item,
                   encoder: JSONParameterEncoder.json(encoder: encoder)
        ).response { [weak self ] response in
            guard response.error == nil else {
                self?.displayFailure()
           
                return
            }
            self?.displaySucces()
        debugPrint(response)
        }
       
        
        
    }
    func sendReguestWithUrlSession(){
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts")else {return}
        var reguest = URLRequest(url: url)
        reguest.httpMethod = "POST"
        reguest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let body : [String:Any] = [
            "birth": 1987,
            "occupation": "Artist",
            "name": "Kiyohara",
            "lastname": "Yukinobu",
            "country": "Russia"
        ]
        do {
            reguest.httpBody = try JSONSerialization.data(withJSONObject: body, options: [])
        }catch let err{
          print("Error",err)
            displayFailure()
        }
        URLSession.shared.dataTask(with: reguest) { (data,response,error) in
            if let error = error {
                self.displayFailure()
                print("error",error)
                return
            }
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as?[String:Any] {
                        print(json)
                    }
                }catch let error {
                    self.displayFailure()
                    print(error,"error")
                }
            }
        }.resume()
    }
      
    
  
    
    private func displaySucces () {
        display.text = "Succes"
        display.textColor = .systemGreen
        let alert = UIAlertController(title: "Succes", message: "Congrats", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "back", style: .cancel, handler: { action in
            print("action")
        }))
        present(alert, animated: true, completion: nil)
        
    }
    
    private func displayFailure () {
        display.text = "Failure"
        display.textColor = .red
        let alert = UIAlertController(title: "Failure", message: "think another way", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "no way", style: .cancel, handler: { action in
           print(action)
            
        }))
        present(alert, animated: true, completion: nil)

    }
    @IBAction func searchFieldTap(_ sender: UITextField) {
        if searchTextfield_LbL.text == nil {
            displayFailure()
        }else{
            displaySucces()
        }
    }
    
    @IBAction func urlSessiontapped(_ sender: UIButton) {
        sendReguestWithUrlSession()
        
    }
    
    @IBAction func alamofire(_ sender: UIButton) {
        sendReguestWithAlamofire()
    }
    
    }


