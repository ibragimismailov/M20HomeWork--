//
//  Decode.swift
//  Home19
//
//  Created by Abraam on 16.01.2022.
//

import Foundation
//class Decode {
//    let userId:Int
//    let id:Int
//    let title:String
//    let body:String
//    init(userId:Int,id:Int,title:String,body:String) {
//        self.userId = userId
//        self.id = id
//        self.title = title
//        self.body = body
//    }
public class Decode:Codable {
    public var id :Int?
    public var title:String?
    
    enum CodingKeys:String,CodingKey {
        case id = "id"
        case title = "title"
    }
    
    init(id :Int,title:String) {
        self.id = id
        self.title = title
    }

    required public init?(dictionary:NSDictionary ){
        id = dictionary["id"] as?Int
        title = dictionary["title"] as? String
    }
    required public init(from decoder:Decoder)throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
    }
}

