//
//  Studs+CoreDataProperties.swift
//  
//
//  Created by Abraam on 05.02.2022.
//
//

import Foundation
import CoreData


extension Studs {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Studs> {
        return NSFetchRequest<Studs>(entityName: "Studs")
    }

    @NSManaged public var studBirth: String?
    @NSManaged public var studCountry: String?
    @NSManaged public var studName: String?
    @NSManaged public var studSirName: String?

}
