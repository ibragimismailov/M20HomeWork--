//
//  AppDelegate.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//


import UIKit
import CoreData
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
   var window : UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }
    lazy var persistentConteiner:NSPersistentContainer = {
        let conteiner = NSPersistentContainer(name: "Model")
        conteiner.loadPersistentStores(completionHandler: {(storeDescriptin,error) in
            if let error = error as NSError? {
                fatalError("Unresolved Error \(error),\(error.userInfo)")
              }
          })
        return conteiner
    }()
    func saveContext (){
        let context = persistentConteiner.viewContext
        if context.hasChanges{
            do{
                try context.save()
            }catch{
                let nsError = error as NSError
                fatalError("Unresolved Error \(nsError),\(nsError.userInfo)")
            }
        }
    }
}
    


