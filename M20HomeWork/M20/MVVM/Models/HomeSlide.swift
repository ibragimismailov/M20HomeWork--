//
//  HomeSlide.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//

import Foundation
import UIKit
struct HomeSlide {
    let title:String
    let description:String
    let image:UIImage
}
