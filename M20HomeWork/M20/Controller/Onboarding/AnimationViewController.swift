//
//  ViewController.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//

import UIKit

class AnimationViewController: UIViewController {
    let d = UserDefaults.standard

   
    
    private let imageView:UIImageView = {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
        imageView.image = UIImage(named: "logo")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(imageView)
        DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
            self.animate()
            if UserDefaults.standard.value(forKey: "adminemail") != nil {

            }
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageView.center = view.center
    }
    private func animate () {
        UIView.animate(withDuration: 0.8, animations:{
            let size = self.view.frame.width * 18
            let xx = size - self.view.frame.size.width
            let yy = self.view.frame.size.height - size
            self.imageView.frame = CGRect(x: -(xx/2),
                                          y: yy/2,
                                          width: size,
                                          height: size)
        } )
        
        UIView.animate(withDuration: 1.5, animations:{
            self.imageView.alpha = 0
        },completion: {done in
            if done {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {

      let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    vc . modalPresentationStyle = .fullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                    
                })
              
                
            }
        }  )
        
    
    }

}

