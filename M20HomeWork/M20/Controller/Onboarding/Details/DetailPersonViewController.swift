//
//  DetailPersonViewController.swift
//  M20
//
//  Created by Abraam on 04.02.2022.
//

import UIKit

class DetailPersonViewController: UIViewController {

    @IBOutlet weak var nameDetail: UILabel!
    @IBOutlet weak var sirNameDetail: UILabel!
    @IBOutlet weak var birthDetail: UILabel!
    @IBOutlet weak var countryDetail: UILabel!
    var students:Studs?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let s = students {
            nameDetail.text = s.studName
            sirNameDetail.text = s.studSirName
            birthDetail.text = s.studBirth
            countryDetail.text = s.studCountry
            
        }
    }
    



}
