//
//  UpdatePersonViewController.swift
//  M20
//
//  Created by Abraam on 04.02.2022.
//

import UIKit
import CoreData
class UpdatePersonViewController: UIViewController {
    let context = appDelegate.persistentConteiner.viewContext
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var sirNameTextField: UITextField!
    @IBOutlet weak var birthTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    var students:Studs?
    override func viewDidLoad() {
        super.viewDidLoad()

        if let s = students {
            nameTextField.text = s.studName
            sirNameTextField.text = s.studSirName
            birthTextField.text = s.studBirth
            countryTextField.text = s.studCountry
            
        }
    }
    

   
    @IBAction func updatePersonButton(_ sender: UIButton) {
        if let s = students , let name = nameTextField.text,let sirname = sirNameTextField.text,let birth = birthTextField.text,let country = countryTextField.text{
            s.studName = name
            s.studSirName = sirname
            s.studBirth = birth
            s.studCountry  = country
            appDelegate.saveContext()
        }
        
    }
    
}
