//
//  AddPersonViewController.swift
//  M20
//
//  Created by Abraam on 04.02.2022.
//

import UIKit
import CoreData
class AddPersonViewController: UIViewController {
    let context = appDelegate.persistentConteiner.viewContext
    @IBOutlet weak var nametextField: UITextField!
    @IBOutlet weak var birthTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var sirNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }

    

    @IBAction func addPersonButton(_ sender: Any) {
        
          if let name = nametextField.text,let sirname = sirNameTextField.text,let birth = birthTextField.text,let country = countryTextField.text {
              if name.count == 0,sirname.count == 0,birth.count == 0,country.count == 0 {
                  let alert  = UIAlertController(title: "Empty", message: "you shold write all of list", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Ok", style: .cancel))
                  present(alert, animated: true)
             
              }else {
                  let student = Studs(context: context)
                  student.studName = name
                  student.studSirName = sirname
                  student.studBirth = birth
                  student.studCountry = country
                  appDelegate.saveContext()
              }

              
          }
     
}


}
