//
//  LoginViewController.swift
//  M20
//
//  Created by Abraam on 24.01.2022.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textEmailTextField: UITextField!
    @IBOutlet weak var textPasswordlTextField: UITextField!
    let def = UserDefaults.standard
    var email:String?
    var password:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let adminemail = def.string(forKey: "adminemail") ?? "nil"
        let adminPassword = def.string(forKey: "adminPassword") ?? "nil"
        if adminemail != "nil" && adminPassword != "nil" {
            performSegue(withIdentifier: "gotoAdmin", sender: nil)
            
        }
       

        
    }


    @IBAction func loginClick(_ sender: UIButton) {
        if let adminemail = textEmailTextField.text,let adminPassword = textPasswordlTextField.text {
            if adminemail == "admin@mail.ru" && adminPassword == "123456" {
                def.set(adminemail, forKey: "adminemail")
                def.set(adminPassword, forKey: "adminPassword")
                performSegue(withIdentifier: "gotoAdmin", sender: nil)
            }else {
                let alert = UIAlertController(title: "Alert", message: "Wrong Email or Password! Please check it again..", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    

    @IBAction func forgotClick(_ sender: UIButton) {
    }
    @IBAction func aboutClick(_ sender: UIButton) {
    }
    @IBAction func registerclick(_ sender: UIButton) {
    }
    
}
