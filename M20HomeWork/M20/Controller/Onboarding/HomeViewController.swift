//
//  HomeViewController.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var nextBTn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    var slides : [HomeSlide] = []
    var currentPage = 0 {
        didSet {
            if currentPage == slides.count - 1 {
                nextBTn.setTitle("Start", for: .normal)
                
            }else {
                nextBTn.setTitle("Next", for: .normal)
            }
        }
    }

        
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        slides = [
            HomeSlide(title: "ibo's jewelery", description: "this jev is so beaituful jev and i say that you should buy it", image: UIImage(named: "jev1")!),
            HomeSlide(title: "for men jevellery", description: "mens are also using jevellery if you also want to but ,you can just click next button", image: UIImage(named: "jev2")!),
            HomeSlide(title: "for woman jevellery", description: "woman are also using jevellery if you alsoda da da da da da want to but ,you can just click next button", image: UIImage(named: "jev3")!)
        
        ]
   
    }

    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        if currentPage == slides.count - 1 {
            let controller = storyboard?.instantiateViewController(withIdentifier: "navVc")as!UINavigationController
            controller.modalPresentationStyle = .fullScreen
            controller.modalTransitionStyle = .flipHorizontal
            present(controller, animated: true, completion: nil)
            
        }else{
            currentPage += 1
            pageControl.currentPage = currentPage
            let indexPath = IndexPath(item: currentPage, section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
       
    }
}
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCVC", for: indexPath)as!HomeCollectionViewCell
        cell.setup(slides[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width =  scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x / width )
        pageControl.currentPage = currentPage
       
        
        
    }
    
    
}
