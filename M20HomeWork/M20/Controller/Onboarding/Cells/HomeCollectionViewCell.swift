//
//  HomeCollectionViewCell.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    static let identifier = "hom"
    @IBOutlet weak var collectionImage: UIImageView!
    @IBOutlet weak var collectionTitle: UILabel!
    @IBOutlet weak var collectionDescription: UILabel!
    
    func setup (_ slide:HomeSlide){
        collectionImage.image = slide.image
        collectionTitle.text = slide.title
        collectionDescription.text = slide.description
        
    }
}

