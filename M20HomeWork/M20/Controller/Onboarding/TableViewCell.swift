//
//  TableViewCell.swift
//  M20
//
//  Created by Abraam on 27.01.2022.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var collectionView: UICollectionView!
    var list:[Product] = [Product]()
    func configure(items:[Product]){
        list = items
        collectionView.delegate = self
        collectionView.dataSource = self
        setLayout()
        
    }
    func setLayout(){
        let Flayout = UICollectionViewFlowLayout ()
        Flayout.itemSize = CGSize(width: self.collectionView.frame.width / 2 - 10, height: self.collectionView.frame.width / 2 - 10)
        Flayout.minimumLineSpacing = 5
        Flayout.minimumInteritemSpacing = 5
        Flayout.scrollDirection = .horizontal
        Flayout.sectionInset = UIEdgeInsets(top: 60 , left: 10, bottom: 10, right: 10)
        self.collectionView.setCollectionViewLayout(Flayout, animated: true)
        
        
    }
}
extension TableViewCell :UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.contentView.backgroundColor = .systemGray4
        return cell
    }
    
    
}
