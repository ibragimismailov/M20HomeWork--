//
//  TableViewController.swift
//  M20
//
//  Created by Abraam on 27.01.2022.
//

import UIKit




class TableViewController: UITableViewController {
var list = [Product]()
    override func viewDidLoad() {
        navigationController?.navigationBar.prefersLargeTitles = true
       
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        for i in 0...9 {
            let model = Product()
            model.name = "header\(i + 1)"
            
            for s in 0...5 {
                let sub_model = Product()
                sub_model.name = "product_header\(s + 1)"
                model.sub_product.append(sub_model)
            }
            list.append(model)
            
        }
       
    
    }
   

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return list.count
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header")
        cell?.textLabel?.text = list[section].name
        cell?.textLabel?.textAlignment = .center
        cell?.textLabel?.textColor = .black
        cell?.textLabel?.font = UIFont(name: "verdana", size: 20)
        cell?.textLabel?.alpha = 0.6
        
        
       
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)as! TableViewCell
        cell.configure(items: list[indexPath.section].sub_product)
        cell.layer.borderWidth = 0.055
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    @IBAction func logout(_ sender: UIBarButtonItem) {
        UserDefaults.standard.removeObject(forKey: "adminemail")
        exit(-1)
    }
    

}
class Product {
    var name:String?
    var sub_product:[Product] = [Product]()
}
