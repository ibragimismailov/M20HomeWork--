//
//  StudentViewController.swift
//  M20
//
//  Created by Abraam on 04.02.2022.
//

import UIKit
import CoreData
let appDelegate = UIApplication.shared.delegate as! AppDelegate
class StudentViewController: UIViewController {
    let context = appDelegate.persistentConteiner.viewContext
 
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
   var studentList = [Studs]()
    override func viewDidLoad() {
        super.viewDidLoad()
tableView.delegate = self
tableView.dataSource = self
        searchBar.delegate = self
        
      
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchStudents()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index = sender as? Int
        if segue.identifier == "toDetail" {
            let vc = segue.destination as! DetailPersonViewController
            vc.students = studentList[index!]
        }
        if segue.identifier == "toUpdate"{
            let vc = segue.destination as! UpdatePersonViewController
            vc.students = studentList[index!]
            
        }
    }
    func fetchStudents(){
        do {
            studentList = try context.fetch(Studs.fetchRequest())
        }catch{
            print("Fetch  Eror")
        }
     
    }
    
    @IBAction func logoutButton(_ sender: UIBarButtonItem) {
        UserDefaults.standard.removeObject(forKey: "adminemail")
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AnimationViewController")
        let navVC = UINavigationController(rootViewController: vc)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true, completion: nil)
        
    }
    func searchStudents(studName:String){
        let fetchReguest :NSFetchRequest<Studs> = Studs.fetchRequest()
        fetchReguest.predicate  = NSPredicate(format: "studName == %@", studName)
        do {
            studentList = try context.fetch(fetchReguest)
        }catch{
            print("Error")
        }
        
    }
    

    @IBAction func add(_ sender: Any) {
        performSegue(withIdentifier: "AddPersonViewController", sender: self)
        

    }
}
extension StudentViewController:UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  studentList.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let student = studentList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as!TableViewCell
        cell.namePerson.text = "\(student.studName ?? "ok") \(student.studSirName ?? "ok")"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toDetail", sender: indexPath.row)
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") {( action, view, handler) in
            let student = self.studentList[indexPath.row]
            self.studentList.remove(at: indexPath.row)
            self.context.delete(student)
            self.fetchStudents()
            appDelegate.saveContext()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
       
        }
        let updateAction = UIContextualAction(style: .normal, title: "Update") { (update,_,_) in
            update.backgroundColor = .systemGray
            self.performSegue(withIdentifier: "toUpdate", sender: indexPath.row)
            
        }
        let swipeAction = UISwipeActionsConfiguration(actions: [action,updateAction])
        appDelegate.saveContext()
        return swipeAction
    }
                                   
}
extension StudentViewController:UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            fetchStudents()
        }else {
            searchStudents(studName: searchText)
            print(searchText)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

    
        
    }
}
