//
//  SceneDelegate.swift
//  M20
//
//  Created by Abraam on 23.01.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
var storyBoard = UIStoryboard(name: "Main", bundle: nil)

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
      
        guard let windowScene = (scene as? UIWindowScene) else { return }

        self.window = UIWindow(windowScene:windowScene)
        let storyB  = UIStoryboard(name: "Main", bundle: nil)
        guard let rootVC = storyB.instantiateViewController(identifier: "TableViewController") as? TableViewController else {
                   print("ViewController not found")
                   return
               }
        guard let initialVC = storyB.instantiateViewController(identifier: "viewController") as? AnimationViewController else {
                   print("ViewController not found")
                   return
               }
        if UserDefaults.standard.value(forKey: "adminemail") != nil {
            let rootNC = UINavigationController(rootViewController: rootVC)
                    self.window?.rootViewController = rootNC
                    self.window?.makeKeyAndVisible()
        }else {
            let roota = UINavigationController(rootViewController: initialVC)
                    self.window?.rootViewController = roota
                    self.window?.makeKeyAndVisible()
            
        }
       
        
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window = self.window
//        let signinPage = self.storyBoard.instantiateViewController(withIdentifier: "TableViewController") as! TableViewController
//        let appDelegade = UIApplication.shared.delegate
//        if UserDefaults.standard.value(forKey: "adminemail") != nil {
//            appDelegade?.window??.rootViewController = signinPage
//        }else {
//            let homePage = self.storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//            appDelegade?.window??.rootViewController = homePage
//        }
        
       
        
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }




